/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
// import Footers from './Menu/Footers';
import Homes from './Menu/Home/Homes';
import Favorite from './Menu/Favorite/Favorite';
import Detail from './Menu/Home/Detail';
import Search from './Menu/Search/Search';

const Router = createStackNavigator({
  Homes:{
    screen :Homes
  },
  Favorite:{
    screen:Favorite
  },
  Detail:{
    screen:Detail
  },
  Search:{
    screen:Search
  }
},{initialRouteName:'Homes'})

export default createAppContainer(Router);
