import React, {Component} from 'react';
import {View} from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text } from 'native-base';
import {withNavigation} from 'react-navigation';

class Footers extends Component{
  render(){
    return(
      <View>
        {/* <Header /> */}
        <Content />
        <Footer>
          <FooterTab>
            <Button vertical onPress={()=>{this.props.navigation.navigate('Homes')}}>
              <Icon name="home" />
              <Text>Home</Text>
            </Button>
            <Button vertical onPress={()=>{this.props.navigation.navigate('Search')}}>
              <Icon name="search"/>
              <Text>Search</Text>
            </Button>
            <Button vertical onPress={()=>{this.props.navigation.navigate('Favorite')}}>
              <Icon active name="star" />
              <Text>Favorite</Text>
            </Button>
            <Button vertical>
              <Icon name="settings" />
              <Text>Setting</Text>
            </Button>
          </FooterTab>
        </Footer>
      </View>
    );
  }
}

export default withNavigation(Footers);