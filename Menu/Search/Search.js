import React, {Component} from 'react';
import {Card, CardItem,Grid,Col,Text, Container, Content,Form,Input,Item,Label, Button, View,Left,Right,Icon} from 'native-base';
import {Image,TouchableOpacity} from 'react-native'
import Footers from '../Footers';
import axios from 'axios';

const API_KEY ='12ac8f7a364983e9334d18873c2f6b5d';

class Search extends Component{
  
  constructor(){
    super()
    this.state={
      textCari:[],
      dataCari:[]
    }
  }

  getCari=(find)=>{
    axios.get(`https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&query=${find}`)
    .then((response)=>{
      this.setState({
        dataCari:response.data.results
      })
      // console.log(response.data.results)
    })
  }

    render(){
        return (
          <View style={{flex:1}}>
              <Form style={{ flexDirection: "row" }}>
                <Item floatingLabel style={{ flex: 2 }}>
                  <Label>Search</Label>
                  <Input
                    // style={styles.textInputStyle}
                    placeholder="Enter Name"
                    returnKeyLabel = {"next"}
                    onChangeText={(text) => this.setState({textCari:text})}
                  />
                </Item>
                <Button onPress={()=>{this.getCari(this.state.textCari)}} style={{ flex: 1, marginTop:20}}><Text>Cari</Text></Button>
              </Form>
              <Content>
                {this.state.dataCari.map((data,key)=>{
                  return(
                    <TouchableOpacity 
                    key={key} 
                    onPress={()=>{
                      this.props.navigation.navigate("Detail",{
                        title:data.original_title,
                        id:data.id
                      })
                      }
                    }
                  >
                    <Card>
                    <CardItem>
                      <Text>{data.original_title}</Text>
                    </CardItem>
                    <CardItem cardBody>
                      <Image
                        style={{height:600,width:null,flex:1}}
                        source={{ uri: 'http://image.tmdb.org/t/p/w342'+data.poster_path }}
                      />
                    </CardItem>
                    <CardItem>
                      <Text>{data.overview}</Text>
                    </CardItem>
                    <CardItem>
                      <Left>
                        <Icon style={{color:"#f7ce31"}} name="star"/>
                        <Text>{data.vote_average}</Text>
                      </Left>
                      <Right>
                        <Text>{data.release_date}</Text>
                      </Right>
                    </CardItem>
                  </Card>
                    </TouchableOpacity>
                  );
                  // console.log(data.original_title)
                })}
              </Content>
            <Footers/>
          </View>
        );
    }
}

export default Search;