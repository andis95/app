import React, {Component} from 'react';
import { View, Text, Container,Content,Card,CardItem ,Left, Right,Icon} from 'native-base';
import {Image} from 'react-native'
import Footers from '../Footers';
import axios from 'axios';

const API_KEY ='12ac8f7a364983e9334d18873c2f6b5d';

const getLogin = (token)=>{
    // console.log(token)
    const databody = JSON.stringify({
        "username": "andis95",
        "password": "andisadapotto",
        "request_token": token
      });
    return fetch(`https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=${API_KEY}`, {
        method:'POST',
        headers: {
            'content-type':'application/json',
        },
        body: databody 
    })
        .then((res)=>res.json())
        .then((res)=>{
            if(res){
                return res
            }
        })
}

const getListFavorite=(session)=>{
    // console.log(session)
    return fetch(`https://api.themoviedb.org/3/account/8711032/favorite/movies?api_key=${API_KEY}&session_id=${session}`,{
        method:'GET',
        headers:{
            'content-type':'application/json',
        }
    })
        .then((res)=>res.json())
        .then((res)=>{
            if(res){
                return res.results
            }
        })
}

const getSession = (token)=>{
    // console.log(token)
    const databody = JSON.stringify({
        "request_token": token
      });
    return fetch(`https://api.themoviedb.org/3/authentication/session/new?api_key=${API_KEY}`, {
        method:'POST',
        headers: {
            'content-type':'application/json',
        },
        body: databody 
    })
        .then((res)=>res.json())
        .then((res)=>{
            if(res){
                return res.session_id
            }
        })
}

const getToken=()=>{
    return fetch(`https://api.themoviedb.org/3/authentication/token/new?api_key=${API_KEY}`)
        .then((res)=>res.json())
        .then((res)=>{
            if(res){
                return res.request_token
            }
        })
}

class Favorite extends Component{

    constructor(){
        super();
        this.state={
            dataFavorite:[],
            dataToken:[],
            dataSession:[]
        }
    }
    
    // getListFavorit=()=>{
    //     axios.get(`https://api.themoviedb.org/3/authentication/token/new?api_key=12ac8f7a364983e9334d18873c2f6b5d`)
    //         .then((response)=>{
    //             this.setState({
    //                 dataToken:response.data.request_token
    //             })
    //         })
    // }

    parsingData=()=>{
        return Promise.all([
            getToken()
        ]).then(([token]) => {
            if(token){
                getLogin(token).then((login)=>{
                    if(login){
                        getSession(token).then((session)=>{
                            // console.log(session)
                            getListFavorite(session).then((data)=>{
                                // console.log(data)
                                this.setState({
                                    dataFavorite:data
                                })
                            })
                        })
                    }
                })
            }
        })
    }

    componentDidMount(){
        // this.getListFavorit();
        // this.getToken();
        this.parsingData();
    }
    
    static navigationOptions={header:null};

    render(){
        return(
            <View style={{flex:1}}>
                <View style={{flex:1}}>
                    <Container>
                    {/* <Text>{JSON.stringify(this.state.dataFavorite)}</Text> */}
                    <Content>
                    {this.state.dataFavorite.map((data,key)=>{
                        return(
                            // <Text>{data.original_title}</Text>
                             <Card>
                                <CardItem>
                                <Text>{data.original_title}</Text>
                                </CardItem>
                                <CardItem cardBody>
                                <Image
                                    style={{height:600,width:null,flex:1}}
                                    source={{ uri: 'http://image.tmdb.org/t/p/w342'+data.poster_path }}
                                />
                                </CardItem>
                                <CardItem>
                                <Text>{data.overview}</Text>
                                </CardItem>
                                <CardItem>
                                <Left>
                                    <Icon style={{color:"#f7ce31"}} name="star"/>
                                    <Text>{data.vote_average}</Text>
                                </Left>
                                <Right>
                                    <Text>{data.release_date}</Text>
                                </Right>
                                </CardItem>
                            </Card>
                        );
                    })}
                    </Content>
                    </Container>
                </View>
                <Footers/>
            </View>

        );
    }
}

export default Favorite;