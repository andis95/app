import React, {Component} from 'react';
import { View, Text, StatusBar } from 'react-native';
import Footers from '../Footers';
import Tabmovies from './Tabmovies';

class Homes extends Component{

    static navigationOptions={header:null};
    render(){
        return(
            <View style={{flex:1}}>
                <StatusBar backgroundColor="#1c4ae3"/>
                <View style={{flex:1}}>
                    <Tabmovies/>            
                </View>
                <Footers/>
            </View>
        );
    }
}

export default Homes;