import React, {Component} from 'react';
import { Container, Header, Tab, Tabs, TabHeading, Text, Card, CardItem, Icon } from 'native-base';
import { Content, List, ListItem, Thumbnail, Left, Body, Right, Button} from 'native-base';
import {Image, TouchableOpacity} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import ImageSlider from 'react-native-image-slider';
import axios from 'axios';
// import { TouchableOpacity } from 'react-native-gesture-handler';
import {withNavigation} from 'react-navigation';
const API_KEY='12ac8f7a364983e9334d18873c2f6b5d';
class Tabmovies extends Component{

  constructor(){
    super();
    this.state={
        dataPlayNow:[],
        dataUpcoming:[]
    }
  }

  getDataNowPlaying=()=>{
    axios.get(`https://api.themoviedb.org/3/movie/now_playing?api_key=${API_KEY}`)
      .then((response)=>{
        this.setState({
          dataPlayNow:response.data.results
        })
      })
  }

  getDataUpcoming=()=>{
    axios.get(`https://api.themoviedb.org/3/movie/upcoming?api_key=${API_KEY}`)
      .then((response)=>{
        this.setState({
          dataUpcoming:response.data.results
        })
      })
  }

  componentDidMount(){
    this.getDataNowPlaying();
    this.getDataUpcoming();
  }
    render(){
        return(
            <Container>
            {/* <Header hasTabs /> */}
            <Tabs>
              <Tab heading="Now Playing">
                {/* <ImageSlider autoPlayWithInterval={3000} images={this.state.images}/> */}
                <Content>
                  {this.state.dataPlayNow.map((data,key)=>{
                    return(
                    <TouchableOpacity 
                      key={key} 
                      onPress={()=>{
                        this.props.navigation.navigate("Detail",{
                          title:data.original_title,
                          id:data.id
                        })
                        }
                      }
                    >
                      <Card>
                        <CardItem>
                          <Text>{data.original_title}</Text>
                        </CardItem>
                        <CardItem cardBody>
                          <Image
                            style={{height:600,width:null,flex:1}}
                            source={{ uri: 'http://image.tmdb.org/t/p/w342'+data.poster_path }}
                          />
                        </CardItem>
                        <CardItem>
                          <Text>{data.overview}</Text>
                        </CardItem>
                        <CardItem>
                          <Left>
                            <Icon style={{color:"#f7ce31"}} name="star"/>
                            <Text>{data.vote_average}</Text>
                          </Left>
                          <Right>
                            <Text>{data.release_date}</Text>
                          </Right>
                        </CardItem>
                      </Card>
                    </TouchableOpacity>
                    );
                  })}
                </Content>
              </Tab>
              <Tab heading="Upcoming">
                <Content>
                  {this.state.dataUpcoming.map((data,key)=>{
                    return(
                      <TouchableOpacity
                        key={key} 
                        onPress={()=>{
                          this.props.navigation.navigate("Detail",{
                            title:data.original_title,
                            id:data.id
                          })
                          }
                        }
                      >
                        <Card>
                        <CardItem>
                          <Text>{data.original_title}</Text>
                        </CardItem>
                        <CardItem cardBody>
                          <Image
                            style={{height:600,width:null,flex:1}}
                            source={{ uri: 'http://image.tmdb.org/t/p/w342'+data.poster_path }}
                          />
                        </CardItem>
                        <CardItem>
                          <Text>{data.overview}</Text>
                        </CardItem>
                        <CardItem>
                          <Left>
                            <Icon style={{color:"#f7ce31"}} name="star"/>
                            <Text>{data.vote_average}</Text>
                          </Left>
                          <Right>
                            <Text>{data.release_date}</Text>
                          </Right>
                        </CardItem>
                      </Card>

                      </TouchableOpacity>
                    );
                  })}
                </Content>
              </Tab>
            </Tabs>
          </Container>
        );
    }
}

export default withNavigation(Tabmovies);