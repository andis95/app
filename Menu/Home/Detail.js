import React, {Component} from 'react';
import { View, Content, Text, Card, CardItem, Left, Icon, Right, Button,Toast, Container,Root} from 'native-base';
import {Image} from 'react-native';
import Footers from '../Footers';
import axios from 'axios';

const API_KEY='12ac8f7a364983e9334d18873c2f6b5d'
// var id ="";

const getLogin = (token)=>{
    // console.log(token)
    const databody = JSON.stringify({
        "username": "andis95",
        "password": "andisadapotto",
        "request_token": token
      });
    return fetch(`https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=${API_KEY}`, {
        method:'POST',
        headers: {
            'content-type':'application/json',
        },
        body: databody 
    })
        .then((res)=>res.json())
        .then((res)=>{
            if(res){
                return res
            }
        })
}

const addListFavorite=(session,id)=>{
    // console.log(id)
    const databody=JSON.stringify({        
        "media_type": "movie",
        "media_id": id,
        "favorite": true
    })
    return fetch(`https://api.themoviedb.org/3/account/8711032/favorite?api_key=${API_KEY}&session_id=${session}`,{
        method:'POST',
        headers:{
            'content-type':'application/json;charset=utf-8',
        },
        body:databody
    })
        .then((res)=>res.json())
        .then((res)=>{
            if(res){
                return res
            }
        })
}

const getSession = (token)=>{
    // console.log(token)
    const databody = JSON.stringify({
        "request_token": token
      });
    return fetch(`https://api.themoviedb.org/3/authentication/session/new?api_key=${API_KEY}`, {
        method:'POST',
        headers: {
            'content-type':'application/json',
        },
        body: databody 
    })
        .then((res)=>res.json())
        .then((res)=>{
            if(res){
                return res.session_id
            }
        })
}

const getToken=()=>{
    return fetch(`https://api.themoviedb.org/3/authentication/token/new?api_key=${API_KEY}`)
        .then((res)=>res.json())
        .then((res)=>{
            if(res){
                return res.request_token
            }
        })
}

class Detail extends Component{
    static navigationOptions=({navigation})=>{
        id_video =navigation.getParam("id");
        // console.log(ID)
        return {
            title:navigation.getParam("title")
        }
    };

    constructor(props){
        super(props);
        this.state={
            showToast: false,
            DetailMovie:[],
            dataFavorite:[]
        }
    }


    getDetail=()=>{
        axios.get(`https://api.themoviedb.org/3/movie/${id_video}?api_key=${API_KEY}`)
            .then((res)=>{
                this.setState({
                    DetailMovie:res.data
                })
            })
            // console.log(`https://api.themoviedb.org/3/movie/${id}?api_key=${API_KEY}`)
            // console.log(API_KEY)
    }

    parsingData=()=>{
        return Promise.all([
            getToken()
        ]).then(([token]) => {
            if(token){
                getLogin(token).then((login)=>{
                    if(login){
                        getSession(token).then((session)=>{
                            // console.log(session)
                            addListFavorite(session,this.state.DetailMovie.id).then((data)=>{
                                console.log(data)
                                Toast.show({
                                    text: "Add to favorite",
                                    buttonText: "Success",
                                    position: "top"
                                    })
                            })
                        })
                    }
                })
            }
        })
    }

    componentDidMount(){
        this.getDetail();
    }

    render(){
        // console.log(this.state.DetailMovie.poster_path)
        return (
            <View style={{flex:1}}>
                <Root>
                <Content>
                    <Card cardBody>
                        <CardItem>
                        {/* <Right> */}
                            <Button small onPress={()=>{this.parsingData()}}>
                                {/* <Icon style={{color:"green"}} name="add"></Icon> */}
                                <Text>Add Favorite</Text>
                            </Button>
                        {/* </Right> */}
                        </CardItem>
                        <CardItem>
                            <Image 
                            source={{ uri: `http://image.tmdb.org/t/p/w342/${this.state.DetailMovie.poster_path}` }}
                            style={{height:400,width:null,flex:1}}                            
                            />    
                        </CardItem>
                        <CardItem>
                            {/* <Text>{JSON.stringify(this.state.DetailMovie)}</Text> */}
                            <Text>{this.state.DetailMovie.overview}</Text>
                        </CardItem>
                        <CardItem>
                            <Left>
                                <Icon style={{color:"#f7ce31"}} name="star"/>
                                <Text>{this.state.DetailMovie.vote_average}</Text>
                            </Left>
                            <Icon style={{marginLeft:-60}} name="heart"/>
                            <Text>{this.state.DetailMovie.popularity}</Text>
                            <Right>
                                <Text>{this.state.DetailMovie.release_date}</Text>
                            </Right>
                        </CardItem>
                    </Card>
                </Content>

                </Root>
                <Footers/>
            </View>
        );
    }
}

export default Detail;